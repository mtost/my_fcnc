import sys
import json
import uproot
import os

file_xsec_kfactor = "data/XSection-MC16-13TeV.data" # copied from /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/dev/AnalysisTop/TopDataPreparation/XSection-MC16-13TeV.data on June 4 2024

if len(sys.argv) != 2:
    raise ImportError("Usage: python get_SOW.py <input directory>")


input_dir = sys.argv[1]

file_dict = {}
for subdir, dirs, files in os.walk(input_dir):
    if "periodAllYear" not in subdir:
        directory_name = os.path.basename(subdir)
        file_list = [os.path.join(subdir, file) for file in files]
        file_dict[directory_name] = file_list


# initialize the dictionary
SOW_XS_KF = {}

for dataset in file_dict.keys():
    if dataset == "data": continue
    print("Initilizing dictionary for: {}".format(dataset))
    for file in file_dict[dataset]:

        file_uproot = uproot.open(file)
        
        # get the metadata
        metadata = file_uproot['metadata'].axis().labels()
        dsid = metadata[2]
        
        file_uproot.close()

        if str(dsid) not in SOW_XS_KF.keys():
            SOW_XS_KF[str(dsid)] = {'xsec': 0.0, 'kfactor': 0.0, 'SOW_mc20a': 0.0, 'SOW_mc20d': 0.0, 'SOW_mc20e': 0.0}


# now do the bit where we read cross section and k factor from the file
contents = []
with open(file_xsec_kfactor) as f:
    print("Accesing cross sections and decorating")
    for line in f:
        contents.append(line.split())

    for i in range(0, len(contents)):
        if len(contents[i])<1: continue
        try:
            dsid = int(contents[i][0])
            xs_pd = float(contents[i][1])*1000 # in fb
            kfactor = float(contents[i][2])
        except ValueError:
            continue

        # going line by line to see if we need the information in the current line
        if str(dsid) in SOW_XS_KF.keys():
            SOW_XS_KF[str(dsid)]['xsec'] = xs_pd
            SOW_XS_KF[str(dsid)]['kfactor'] = kfactor

# now go back through all the files, and actually decorate the necesary information
for dataset in file_dict.keys():
    if dataset == "data": continue
    print("Filling SOW dictionary for: {}".format(dataset))
    for file in file_dict[dataset]:

        file_uproot = uproot.open(file)
        
        metadata = file_uproot['metadata'].axis().labels()
        campaign = metadata[1]
        dsid = metadata[2]
        
        #Get the SOW here
        for available in file_uproot.keys():
            if "CutBookkeeper" in available:
                SOW_histo = file_uproot[available]
        SOW = SOW_histo.values()[1]

        file_uproot.close()

        #try:
        #    dsid = file_uproot["nominal"]["DSID"].array()[0]
        #    run_number = file_uproot["nominal"]["runNumber"].array()[0]
        #    SOW = file_uproot["nominal"]["SOW"].array()[0]
        #except:
        #    file_uproot.close()
        #    continue
        #file_uproot.close()

        # make sure that each DSID is completed from the previous step
        if SOW_XS_KF[str(dsid)]['xsec'] == 0.0:
            print("WARNING WARNING found {} in files without valid cross section".format(dsid))

        if campaign == 'mc20a':
            SOW_XS_KF[str(dsid)]['SOW_mc20a'] += SOW
        elif campaign == 'mc20d':
            SOW_XS_KF[str(dsid)]['SOW_mc20d'] += SOW
        elif campaign == 'mc20e':
            SOW_XS_KF[str(dsid)]['SOW_mc20e'] += SOW

# check if anything is missing
for dsid in SOW_XS_KF.keys():
    if SOW_XS_KF[dsid]['SOW_mc20a'] == 0.0:
        print("Warning! {} is missing mc20a sum of weights".format(dsid))
    if SOW_XS_KF[dsid]['SOW_mc20d'] == 0.0:
        print("Warning! {} is missing mc20d sum of weights".format(dsid))
    if SOW_XS_KF[dsid]['SOW_mc20e'] == 0.0:
        print("Warning! {} is missing mc20e sum of weights".format(dsid))


#print(SOW_XS_KF)

print("Writing output json..")
with open("data/SOW_v1.json", "w") as outfile:
    json.dump(SOW_XS_KF, outfile, indent=2)

