import os
import uproot
import pickle
from datetime import datetime

def get_all_files(data_directory, from_saved = False):

    if from_saved:
        # Find the most recent pickle file in the data directory
        pickle_files = [f for f in os.listdir("data") if f.endswith('.pkl')]
        latest_file = max(pickle_files, key=lambda x: os.path.getctime(os.path.join("data", x)))
        latest_file_path = os.path.join("data", latest_file)
        
        # Load the fileset from the most recent pickle file
        with open(latest_file_path, 'rb') as f:
            fileset = pickle.load(f)
        print(f"Loaded fileset from {latest_file_path}")

        return fileset

    else:

        root_files_dict = {}

        for dirpath, dirnames, filenames in os.walk(data_directory):
            root_files = [file for file in filenames if file.endswith('.root')]
            if root_files:
                directory_key = os.path.basename(dirpath)
                print("Adding files from: {}".format(directory_key))
                root_files_dict[directory_key] = [os.path.join(dirpath, file) for file in root_files]

        fileset = check_if_good(root_files_dict)

        
        # Generate the filename with today's date
        today_date = datetime.now().strftime("%Y-%m-%d")
        filename = os.path.join("data", f"fileset_{today_date}.pkl")

        # Save the fileset dictionary using pickle
        with open(filename, 'wb') as f:
            pickle.dump(fileset, f)
        print(f"Fileset saved to {filename}")

        return fileset


def check_if_good(fileset):
    
    valid_files_dict = {}
    for directory, files in fileset.items():
        directory = clean_name(directory)
        print("Validating files in {}".format(directory))
        valid_files = []
        for file in files:
            with uproot.open(file) as opened_file:
                if "reco" in opened_file:
                    tree = opened_file["reco"]
                    try:
                        this = tree["el_pt_NOSYS"]
                        valid_files.append(file)
                    except:
                        this = "bad file"
        if valid_files:
            valid_files_dict[directory] = valid_files
    return valid_files_dict
                        

def clean_name(big_name):

    available = ['singletop', 'wjets', 'ttbar', 'diboson', 'grp15', 'grp16', 'grp17', 'grp18']

    for dataset in available:
        if dataset in big_name:
            small_name = dataset

    if small_name not in available:
        small_name = "NotFound"
    
    return small_name