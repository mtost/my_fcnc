import awkward as ak
import numpy as np
import json
from hist import Hist
import uproot


def general_fcnc(file):

    # get the histogram dictionary 
    histo = make_histos()


    this = uproot.open(file+":reco")
    # This line takes a super long time, work on that
    events = this.arrays(filter_name = ["*pt*", "*phi", "*eta*", "*_e_NOSYS", "met_*", "*prob*", "weight_*", "globalTrigger*", "*Number"], library="ak")

    # lepton selection
    n_el = ak.num(events.el_pt_NOSYS) == 1
    n_mu = ak.num(events.mu_pt_NOSYS) == 1
    n_lep = n_el | n_mu
    cut_lep_pt_loose = ak.concatenate([events.mu_pt_NOSYS,events.el_pt_NOSYS],axis=1)[:,0]>=25e3

    # jet selection
    fb = 0.2
    fc = 0.2
    GN2_c = np.log( events.jet_prob_pc_GN2v00LegacyWP_NOSYS / ((events.jet_prob_pb_GN2v00LegacyWP_NOSYS * fb) + (events.jet_prob_pl_GN2v00LegacyWP_NOSYS * (1 - fb))) )
    GN2_b = np.log( events.jet_prob_pb_GN2v00LegacyWP_NOSYS / ((events.jet_prob_pc_GN2v00LegacyWP_NOSYS * fc) + (events.jet_prob_pl_GN2v00LegacyWP_NOSYS * (1 - fc))) )
    c_jet_list = events.jet_pt_NOSYS[ak.argsort(GN2_c, axis=1, ascending=False)]
    b_jet_list = events.jet_pt_NOSYS[ak.argsort(GN2_b, axis=1, ascending=False)]
    cut_b_c_overlap = ak.all(c_jet_list != b_jet_list, axis=1)
    cut_1c = ak.sum(GN2_c>1.392, axis=1)>=1
    cut_1b = ak.sum(GN2_b>1.877, axis=1)==1
    cut_3jet = ak.num(events.jet_pt_NOSYS)>=3

    # apply the selection
    events_cut = events[n_lep & cut_lep_pt_loose & cut_3jet & cut_1c & cut_1b & cut_b_c_overlap]

    if len(events_cut) < 1: return histo

    # get the weights:
    weight = get_weights(events_cut)

    # make all required calculations
    lep_pt = ak.concatenate([events_cut.mu_pt_NOSYS, events_cut.el_pt_NOSYS], axis=1)[:, 0]
    lep_eta = ak.concatenate([events_cut.mu_eta, events_cut.el_eta], axis=1)[:, 0]
    lep_phi = ak.concatenate([events_cut.mu_phi, events_cut.el_phi], axis=1)[:, 0]

    #fill the histograms
    histo['lep_pt'].fill(region='test', pt=lep_pt/1000, weight = weight)
    histo['lep_eta'].fill(region='test', eta=lep_eta, weight = weight)
    histo['lep_phi'].fill(region='test', phi=lep_phi, weight = weight)
    histo['njets'].fill(region='test', njets=ak.num(events_cut.jet_pt_NOSYS), weight = weight)
    histo['met'].fill(region='test', met=events_cut.met_met_NOSYS/1000, weight = weight)

    this.close()

    return histo


def make_histos():

    region_axis = Hist.new.StrCat(['test'], name="region", label="region")
    pt_axis = Hist.new.Reg(60, 0, 300, name="pt", label=r"$p_{T}$ [GeV]")
    eta_axis = Hist.new.Reg(50, -2.5, 2.5, name="eta", label=r"$\eta$")
    phi_axis = Hist.new.Reg(70, -3.5, 3.5, name="phi", label=r"$\phi$")
    njets_axis = Hist.new.Int(0, 10, name="njets", label="number of jets")
    met_axis = Hist.new.Reg(60, 0, 300, name="met", label=r"met [GeV]")
    mass_axis = Hist.new.Reg(100, 0, 500, name="mass", label="mass [GeV]")
    mass_long_axis = Hist.new.Reg(30, 0, 1500, name="mass", label="mass [GeV]")
    #delta_phi_axis = Hist.new.Reg(50, 0, 7, name="dphi", label=r"$\delta\phi$")
    #delta_R_axis = Hist.new.Reg(50, 0, 10, name="dR", label=r"$\delta$R")

    histo = {}
    histo["lep_pt"] = Hist(region_axis, pt_axis, storage="weight")
    histo["lep_eta"] = Hist(region_axis, eta_axis, storage="weight")
    histo["lep_phi"] = Hist(region_axis, phi_axis, storage="weight")
    histo["njets"] = Hist(region_axis, njets_axis, storage="weight")
    histo["nbjets"] = Hist(region_axis, njets_axis, storage="weight")
    histo["ncjets"] = Hist(region_axis, njets_axis, storage="weight")
    histo["met"] = Hist(region_axis, met_axis, storage="weight")
    histo["c_pt"] = Hist(region_axis, pt_axis, storage="weight")
    histo["b_pt"] = Hist(region_axis, pt_axis, storage="weight")
    #histo["dphi_met_lep"] = Hist(self.region_axis, self.delta_phi_axis, storage="weight")
    #histo["dR_cb"] = Hist(self.region_axis, self.delta_R_axis, storage="weight")
    #histo["dR_lepb"] = Hist(self.region_axis, self.delta_R_axis, storage="weight")

    return histo


def get_weights(events):

    #placeholder values for now
    #xsec = 1.0
    #lumi = 1.0
    #kfac = 1.0
    #sow = 1.0

    campaign = events.runNumber[0]
    dsid = str(events.mcChannelNumber[0])

    with open('data/SOW_v1.json', 'r') as file:
        SOW_data = json.load(file)

    xsec = SOW_data[dsid]['xsec']
    kfac = SOW_data[dsid]['kfactor']
    if campaign == 284500:
        sow = SOW_data[dsid]['SOW_mc20a']
        lumi = 36.2077
    if campaign == 300000:
        sow = SOW_data[dsid]['SOW_mc20d']
        lumi = 44.3074
    if campaign == 310000:
        sow = SOW_data[dsid]['SOW_mc20e']
        lumi = 58.5401
    
    weights_arr = events['weight_leptonSF_tight_NOSYS'] * events['weight_mc_NOSYS'] * events['weight_beamspot'] * events['weight_jvt_effSF_NOSYS'] * events['weight_pileup_NOSYS'] * events['globalTriggerMatch_NOSYS']
    weights_scalar = (xsec * lumi * kfac) / sow

    weight = weights_arr * weights_scalar

    return weight